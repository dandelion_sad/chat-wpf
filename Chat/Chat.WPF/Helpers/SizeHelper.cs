﻿using System.Windows;

namespace Chat.WPF.Helpers
{
    public class SizeHelper
    {
        /// <summary>
        /// Get height current of window.
        /// </summary>
        /// <returns>Return height when handle.</returns>
        public static double GetHeight()
        {
            var result = Application.Current.MainWindow.ActualHeight - 20;
            return result;
        }

        /// <summary>
        /// Get width current of window.
        /// </summary>
        /// <returns>Return width when handle.</returns>
        public static double GetWidth()
        {
            var result = Application.Current.MainWindow.ActualWidth - 40;
            return result;
        }
    }
}
