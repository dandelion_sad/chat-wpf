﻿using Chat.WPF.Helpers;
using System.Collections.Generic;

namespace Chat.WPF.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {

        #region Properties

        private List<string> listUser;

        public List<string> ListUser { get => listUser; set { listUser = value;OnPropertyChanged(); } }

        #region Set Size Window

        private double actualHeight;

        public double ActualHeight { get { return SizeHelper.GetHeight(); } set { actualHeight = value;OnPropertyChanged(); } }

        private double actualWidth;

        public double ActualWidth { get { return SizeHelper.GetWidth(); } set { actualWidth = value;OnPropertyChanged(); } }

        #endregion Set Size Window

        /* ========================================================== */


        #endregion Properties

        /* ========================================================== */

        #region Commands

        #endregion Commands

        /* ========================================================== */

        #region Constructors

        /// <summary>
        /// Constructor default of home view.
        /// </summary>
        public HomeViewModel()
        {
            InitializeCommands();
        }

        #endregion Constructors

        /* ========================================================== */

        #region Initialize Commands

        /// <summary>
        /// Initialize Command of home view.
        /// </summary>
        public void InitializeCommands()
        {
            ListUser = new List<string>();
            for (int i = 1; i <= 15; i++)
            {
                ListUser.Add($"Hoài {i}");
            }
        }

        #endregion Initialize Commands

    }
}
