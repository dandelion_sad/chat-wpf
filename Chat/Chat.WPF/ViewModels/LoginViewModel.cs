﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace Chat.WPF.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {

        #region Properities

        /* Login */

        private string userNameLogin;

        public string UserNameLogin { get => userNameLogin; set { userNameLogin = value; OnPropertyChanged(); } }

        private string passwordLogin;

        public string PasswordLogin { get => passwordLogin; set { passwordLogin = value; OnPropertyChanged(); } }

        /* /Login */

        /* Singup */

        private string userNameSignup;

        public string UserNameSignup { get => userNameSignup; set { userNameSignup = value; OnPropertyChanged(); } }

        private string passwordSignup;

        public string PasswordSignup { get => passwordSignup; set { passwordSignup = value; OnPropertyChanged(); } }

        private string confirmPasswordSignup;

        public string ConfirmPasswordSignup { get => confirmPasswordSignup; set { confirmPasswordSignup = value; OnPropertyChanged(); } }

        /* /Singup */

        #endregion Properities

        /* ========================================================== */

        #region Commands

        /// <summary>
        /// Command of button login front.
        /// </summary>
        public ICommand LoginFrontCommand { get; set; }

        /// <summary>
        /// Command of button signup front.
        /// </summary>
        public ICommand SingupFrontCommand { get; set; }

        /// <summary>
        /// Command of password change login.
        /// </summary>
        public ICommand PasswordChangeLoginCommand { get; set; }

        /// <summary>
        /// Command of password change signup.
        /// </summary>
        public ICommand PasswordChangeSignupCommand { get; set; }

        /// <summary>
        /// Command of comfirm password change signup.
        /// </summary>
        public ICommand ComfirmPasswordChangeSignupCommand { get; set; }

        #endregion Commands

        /* ========================================================== */

        #region Constructors

        /// <summary>
        /// Constructor default of login view.
        /// </summary>
        public LoginViewModel()
        {
            InitializeCommands();
        }

        #endregion Constructors

        /* ========================================================== */

        #region Initialize Commands
        
        /// <summary>
        /// Initialize commands of login view.
        /// </summary>
        public void InitializeCommands()
        {
            LoginFrontCommand = new RelayCommand<Button>((isEnabled) => {
                if (string.IsNullOrEmpty(UserNameLogin) || string.IsNullOrEmpty(PasswordLogin))
                    return false;
                return true;
            }, (p) => {
                Login(p);
            });

            SingupFrontCommand = new RelayCommand<Button>((isEnabled) => {
                if (string.IsNullOrEmpty(UserNameSignup) || string.IsNullOrEmpty(PasswordSignup) || string.IsNullOrEmpty(ConfirmPasswordSignup))
                    return false;
                return true;
            }, (p) => {
                Signup(p);
            });

            PasswordChangeLoginCommand = new RelayCommand<PasswordBox>((p) => {
                return true;
            }, (password) => {
                PasswordLogin = password.Password;
            });

            PasswordChangeSignupCommand = new RelayCommand<PasswordBox>((p) => {
                return true;
            }, (password) => {
                PasswordSignup = password.Password;
            });

            ComfirmPasswordChangeSignupCommand = new RelayCommand<PasswordBox>((p) => {
                return true;
            }, (password) => {
                ConfirmPasswordSignup = password.Password;
            });
        }

        #endregion Initialize Commands

        /* ========================================================== */

        #region Login

        /// <summary>
        /// Login account.
        /// </summary>
        public void Login(FrameworkElement fe)
        {
            Storyboard sb = (Storyboard)fe.FindResource("LoadFormSignUp");
            sb.Begin();
        }

        #endregion Login

        /* ========================================================== */

        #region Sign up

        /// <summary>
        /// Login account.
        /// </summary>
        public void Signup(FrameworkElement fe)
        {
            Storyboard sb = (Storyboard)fe.FindResource("LoadFormLogin");
            sb.Begin();
        }

        #endregion Sign up
    }
}
