﻿using System.Windows;

namespace Chat.WPF.Views
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        #region Reset input

        private void btnSignupBehind_Click(object sender, RoutedEventArgs e)
        {
            txtPasswordLogin.Password = string.Empty;
            ckbRemember.IsChecked = false;
        }

        private void btnLoginBehind_Click(object sender, RoutedEventArgs e)
        {
            txtPasswordSignup.Password = txtComfirmPasswordSignup.Password = string.Empty;
        }

        #endregion Reset input

    }
}
